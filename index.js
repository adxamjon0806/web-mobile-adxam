let input = "1.1.1.1";
function change(input) {
  let output = "";
  for (let i = 0; i < input.length; i++) {
    const element = input[i];
    if (element === ".") {
      output += `[.]`;
    } else {
      output += element;
    }
  }
  return output;
}

console.log(change(input));
